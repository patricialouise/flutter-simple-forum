import 'package:flutter/material.dart';
import 'package:flutter_simple_forum/ui/PostList.dart';
import 'package:flutter_simple_forum/ui/AddPost.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple Forum',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: PostList(),
      initialRoute: '/',
      routes: {
        '/': (context) => PostList(),
        '/addPost': (context) => AddPost(),
      },
    );
  }
}