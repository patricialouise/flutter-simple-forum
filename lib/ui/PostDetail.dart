import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_simple_forum/bloc/CommentsBlocProvider.dart';
import 'package:flutter_simple_forum/models/CommentsModel.dart';

class PostDetail extends StatefulWidget {
  final int id;
  final int userId;
  final String title;
  final String body;

  PostDetail({
    this.id,
    this.userId,
    this.title,
    this.body,
  });

  @override
  State<StatefulWidget> createState() {
    return PostDetailState(
      id: id,
      userId: userId,
      title: title,
      body: body
    );
  }
}

class PostDetailState extends State<PostDetail> {
  final int id;
  final int userId;
  final String title;
  final String body;

  CommentsBloc bloc;

  PostDetailState({
    this.id,
    this.userId,
    this.title,
    this.body,
  });

  @override
  void didChangeDependencies() {
    bloc = CommentsBlocProvider.of(context);
    bloc.fetchCommentsById(id);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  static const EdgeInsets _allSidesPadding = EdgeInsets.all(10.0);
  static const EdgeInsets _titleMargin =
      EdgeInsets.only(top: 20.0, bottom: 20.0, left: 10.0, right: 10.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Post Detalis'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Container(
        padding: _allSidesPadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: _allSidesPadding,
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: _allSidesPadding,
              child: Text(body),
            ),
            Padding(
              padding: _titleMargin,
              child: Text(
                'Comments',
                style: TextStyle(
                  fontSize: 20.0
                ),
              ),
            ),
            Container(
              child: Expanded(
                child: StreamBuilder(
                stream: bloc.postComments,
                builder: (context, AsyncSnapshot<Future<CommentsModel>> snapshot) {
                  if (snapshot.hasData) {
                    return FutureBuilder(
                      future: snapshot.data,
                      builder: (context,
                          AsyncSnapshot<CommentsModel> itemSnapShot) {
                        if (itemSnapShot.hasData) {
                          if (itemSnapShot.data.comments.length > 0) {
                            return commentsLayout(itemSnapShot.data);
                          }else{
                            return noComments(itemSnapShot.data);
                          }
                        } else {
                          return Center(child: CircularProgressIndicator());
                        }
                      },
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
              ),
            ),
          ],
        )
        ),
      // ),
    );
  }

  Widget noComments(CommentsModel data) {
    return Center(
      child: Container(
        child: Text("No comments Available"),
      ),
    );
  }

  Widget commentsLayout(CommentsModel data) {
    const EdgeInsets _commentsPadding = EdgeInsets.only(top: 10.0, bottom: 10.0);
    var _comments = data.comments;

    return ListView.builder(
      // padding: _allSidesPadding,
      itemCount: _comments.length,
      itemBuilder: (context, i) {
        if(_comments.length > 0){
          return Column(
            children: <Widget>[
              Container(
                padding: _commentsPadding,
                child: _CommentItem(_comments[i]),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey)
                ),
              ),
            ],
          );
        }
      });
  }
}

class _CommentItem extends ListTile {
  static const EdgeInsets _bodyPadding = EdgeInsets.only(top: 20.0);

  _CommentItem(_comment)
      : super(
          title: Text(
            _comment.name,
            style: TextStyle(
              fontWeight: FontWeight.bold
            )),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(_comment.email),
              Padding(
                padding: _bodyPadding,
                child: Text(
                  _comment.body,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          )
        );
}