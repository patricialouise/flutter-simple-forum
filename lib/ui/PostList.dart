import 'package:flutter/material.dart';
import 'package:flutter_simple_forum/models/PostsModel.dart';
import 'package:flutter_simple_forum/bloc/PostsBloc.dart';
import 'package:flutter_simple_forum/ui/PostDetail.dart';
import 'package:flutter_simple_forum/bloc/CommentsBlocProvider.dart';

class PostList extends StatefulWidget {
  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  int _showedCount = 5;
  static const EdgeInsets _titlePadding = EdgeInsets.all(20.0);
  static const EdgeInsets _allSidesPadding = EdgeInsets.all(10.0);

  @override
  void initState() {
    super.initState();
    bloc.fetchAllPosts();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Forum'),
        backgroundColor: Colors.blueAccent,
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Add', 
              style: TextStyle(fontSize: 16.0, color: Colors.white)
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/addPost');
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: _titlePadding,
              child: Text(
                'Posts',
                style: TextStyle(
                  fontSize: 30
                )),
            ),
            Container(
              child: Expanded(
                child: StreamBuilder(
                  stream: bloc.allPosts,
                  builder: (context, AsyncSnapshot<PostModel> snapshot) {
                    if (snapshot.hasData) {
                      return buildList(snapshot);
                    } else if (snapshot.hasError) {
                      return Text(snapshot.error.toString());
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              )
            ),
          ],
        ),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<PostModel> snapshot) {
    var _posts = snapshot.data.posts;

    return ListView.builder(
        padding: _allSidesPadding,
        itemCount: _posts.length,
        itemBuilder: (context, i) {
          if(_posts.length > 0 && i < _showedCount){
            return Column(
              children: <Widget>[
                Divider(),
                _PostListItem(context, _posts[i]),
                i == (_showedCount - 1) ? Divider() : Container(),
                i == (_showedCount - 1) ? (
                  _posts.length > 5 && _showedCount < _posts.length
                      ? Container(
                          margin: _allSidesPadding,  
                          child: FlatButton(
                            textColor: Colors.black54,
                            onPressed: () {
                              int newCount;

                              if ((_showedCount + 5) > _posts.length) {
                                newCount = _posts.length;
                              } else {
                                newCount = this._showedCount + 3;
                              }
                              setState(() {
                                _showedCount = newCount;
                              });
                            },
                            child: Text('Load More'),
                          ),
                        )
                      : Container()
                ) : Container(),
              ],
            );
          }
        });
  }
}

class _PostListItem extends ListTile {
  _PostListItem(context, _post)
      : super(
          title: Text(_post.title),
          onTap: (){
            _openPostDetailPage(_post, context);
          }
        );

  static _openPostDetailPage(Post _post, context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return CommentsBlocProvider(
            child: PostDetail(
            id: _post.id,
            userId: _post.userId,
            title: _post.title,
            body: _post.body,
          ),
        );
      }),
    );
  }
}