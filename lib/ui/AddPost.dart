import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'package:flutter_simple_forum/bloc/PostsBloc.dart';
import 'package:flutter_simple_forum/models/PostsModel.dart';
class AddPost extends StatefulWidget {
  @override
  _AddPostState createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> {
  final _formKey = GlobalKey<FormState>();
  int _status = 0;

  static const double _formDistance = 5.0;
  static const EdgeInsets _allSidesPadding = EdgeInsets.all(10.0);
  static const EdgeInsets _containerPaddingEdgeInset =
      EdgeInsets.only(top: _formDistance, bottom: _formDistance);

  TextEditingController titleController = TextEditingController();
  TextEditingController bodyController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return Scaffold(
      appBar: AppBar(
        title: Text('New Post'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Form(
          key: _formKey,
          child: Container(
            padding: _allSidesPadding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: _containerPaddingEdgeInset,
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a title';
                      }
                    },
                    controller: titleController,
                    decoration: InputDecoration(
                        labelText: 'Title',
                        labelStyle: textStyle,
                        // contentPadding: _allSidesPadding,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  )),
                Padding(
                  padding: _containerPaddingEdgeInset,
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter your post details.';
                      }
                    },
                    controller: bodyController,
                    decoration: InputDecoration(
                        labelText: 'Description',
                        labelStyle: textStyle,
                        // contentPadding: _allSidesPadding,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  )),
                  StreamBuilder(
                    stream: bloc.savePostDetails,
                    builder: (context, AsyncSnapshot<Response> snapshot) {
                      if (snapshot.hasData) {
                        if(snapshot.data.statusCode == 201){
                          return SimpleDialog(
                            title: Text('Post successfully created'),
                            children: <Widget>[
                              SimpleDialogOption(
                                onPressed: () { Navigator.pop(context); },
                                child: Text('OK'),
                              ),
                            ],
                          );
                        }
                      } else if (snapshot.hasError) {
                        return SimpleDialog(
                          title: Text(snapshot.error.toString()),
                          children: <Widget>[
                            SimpleDialogOption(
                              onPressed: () { Navigator.pop(context); },
                              child: Text('OK'),
                            ),
                          ],
                        );
                      }

                      return Container();
                    },
                  )
              ],
            ),
          ),
        ),
        bottomNavigationBar: RaisedButton(
            color: Theme.of(context).primaryColorDark,
            textColor: Theme.of(context).primaryColorLight,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                setState(() {
                  _status = 1; 
                });
                _savePost();
              }
            },
            padding: _allSidesPadding,
            child: _status == 1 ? CircularProgressIndicator() : 
            Text('Submit', textScaleFactor: 1.5),
          ),
    );
  }

  _savePost(){
    String title = titleController.text;
    String body = bodyController.text;
    int userId = 1;

    Post _post = Post(title: title, body: body, userId: userId);
    bloc.savePost(_post);
  }
}
