import 'package:flutter/material.dart';
import 'package:flutter_simple_forum/bloc/PostsBloc.dart';
export 'package:flutter_simple_forum/bloc/PostsBloc.dart';

class CommentsBlocProvider extends InheritedWidget {
  final PostsBloc bloc;
  
  CommentsBlocProvider({Key key, Widget child})
      : bloc = PostsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static PostsBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(CommentsBlocProvider)
            as CommentsBlocProvider)
        .bloc;
  }
}