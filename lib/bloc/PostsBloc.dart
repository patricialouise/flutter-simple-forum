import 'package:http/http.dart';

import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_simple_forum/models/PostsModel.dart';

class PostsBloc {
  final _repository = Repository();
  final _postsFetcher = PublishSubject<PostModel>();
  final _postDetails = PublishSubject<Response>();

  Observable<PostModel> get allPosts => _postsFetcher.stream;
  Observable<Response> get savePostDetails => _postDetails.stream;

  fetchAllPosts() async {
    PostModel postModel = await _repository.fetchAllPosts();
    _postsFetcher.sink.add(postModel);
  }

  savePost(Post post) async {
    Response postModel = await _repository.savePost(post);
    _postDetails.sink.add(postModel);
  }

  dispose() {
    _postDetails.close();
    _postsFetcher.close();
  }
}

final bloc = PostsBloc();