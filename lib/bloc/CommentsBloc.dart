import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:flutter_simple_forum/models/CommentsModel.dart';
import 'package:flutter_simple_forum/resources/repository.dart';

class CommentsBloc {
  final _repository = Repository();
  final _postId = PublishSubject<int>();
  final _comments = BehaviorSubject<Future<CommentsModel>>();

  Function(int) get fetchCommentsById => _postId.sink.add;
  Observable<Future<CommentsModel>> get postComments => _comments.stream;

  CommentsBloc() {
    _postId.stream.transform(_itemTransformer()).pipe(_comments);
  }

  dispose() async {
    _postId.close();
    await _comments.drain();
    _comments.close();
  }

  _itemTransformer() {
    return ScanStreamTransformer(
      (Future<CommentsModel> comments, int id, int index) {
        print(index);
        comments = _repository.fetchComments(id);
        return comments;
      },
    );
  }
}