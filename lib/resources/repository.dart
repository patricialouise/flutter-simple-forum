import 'dart:async';
import 'package:flutter_simple_forum/resources/PostsApiProvider.dart';
import 'package:flutter_simple_forum/models/PostsModel.dart';
// import 'package:flutter_simple_forum/models/PostDetailModel.dart';
import 'package:flutter_simple_forum/models/CommentsModel.dart';
import 'package:http/http.dart';

class Repository {
  final postsApiProvider = PostApiProvider();

  Future<PostModel> fetchAllPosts() => postsApiProvider.fetchPosts();

  Future<CommentsModel> fetchComments(int id) => postsApiProvider.fetchComments(id);

  Future<Response> savePost(Post post) => postsApiProvider.savePost(post);
}