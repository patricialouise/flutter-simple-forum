import 'package:flutter/material.dart';
import 'package:flutter_simple_forum/bloc/CommentsBloc.dart';
export 'package:flutter_simple_forum/bloc/CommentsBloc.dart';

class CommentsBlocProvider extends InheritedWidget {
  final CommentsBloc bloc;
  
  CommentsBlocProvider({Key key, Widget child})
      : bloc = CommentsBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static CommentsBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(CommentsBlocProvider)
            as CommentsBlocProvider)
        .bloc;
  }
}