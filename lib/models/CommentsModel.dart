class CommentsModel {
  final List<Comment> comments;
  
  CommentsModel({this.comments});

  factory CommentsModel.fromJson(List<dynamic> parsedJson){

    List<Comment> comments = new List<Comment>();
    comments = parsedJson.map((i)=>Comment.fromJson(i)).toList();

    return CommentsModel(
      comments: comments
    );
  }
}

class Comment{
  final int postId;
  final int id;
  final String name;
  final String email;
  final String body;

  Comment({this.postId, this.id, this.name, this.email, this.body});

  factory Comment.fromJson(Map<String, dynamic> json){
    return Comment(
      postId: json['postId'],
      id: json['id'],
      name: json['name'],
      email: json['email'],
      body: json['body'],
    );
  }
}