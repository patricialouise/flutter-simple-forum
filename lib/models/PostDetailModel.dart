class PostDetailModel {
  int _id;
  List<_PostDetail> _results = [];

  PostDetailModel.fromJson(Map<String, dynamic> parsedJson) {
    List<_PostDetail> temp = [];

    for (int i = 0; i < parsedJson.length; i++) {
      _PostDetail result = _PostDetail(parsedJson[i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<_PostDetail> get results => _results;
}

class _PostDetail {
  int _id;
  int _userId;
  String _title;
  String _body;


  _PostDetail(result) {
    _id = result['id'];
    _userId = result['_userId'];
    _title = result['title'];
    _body = result['body'];
  }

  int get id => _id;
  int get userId => _userId;
  String get title => _title;
  String get body => _body;
}